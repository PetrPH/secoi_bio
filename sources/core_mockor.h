#ifndef CORE_MOCKOR_H
#define CORE_MOCKOR_H

#include <QImage>
#include <QTime>
#include <QProgressBar>
#include <iostream>

using namespace std;

class Core_mockor{
    public:
        static uint* process(QImage im, int posx, int posy, QProgressBar *progress, bool hue, bool saturation, bool value, bool use_dist, QColor selected = QColor());
        static QImage compose_image(QImage im, uint* data, int thresh, bool soft_color);
        static QImage compose_area(QImage im, uint* data, int thresh);
        static QImage compose_degree(QImage im, uint* data, int thresh);

    private:
        static float m_location(int x1, int y1, int x2, int y2);
        static float m_dist(int x1, int y1, int x2, int y2, int w, int h);
        static float m_color(float sear0, float sear1, float sear2, float act0, float act1, float act2, bool hue, bool saturation, bool value);
};

#endif // CORE_MOCKOR_H
