#include "metrics.h"

float Metrics::dice(QImage *im1, QImage *im2){
    bool *arr1 = new bool[im1->width()*im1->height()];
    bool *arr2 = new bool[im1->width()*im1->height()];
    int el     = im1->width() * im1->height();

    for (int y=0; y<im1->height(); y++){
        int p = y * im1->width();
        for (int x=0; x<im1->width(); x++){
            QRgb a = im1->pixel(x, y);
            arr1[p+x] = abs((int)qRed(a)-(int)qGreen(a))<10 && abs((int)qRed(a)-(int)qBlue(a))<10 && abs((int)qGreen(a)-(int)qBlue(a))<10 ? false : true;
            a = im2->pixel(x, y);
            arr2[p+x] = abs((int)qRed(a)-(int)qGreen(a))<10 && abs((int)qRed(a)-(int)qBlue(a))<10 && abs((int)qGreen(a)-(int)qBlue(a))<10 ? false : true;
        }
    }

    int intersect = 0;
    for (int i=0; i<el; i++){
        if (arr1[i] == arr2[i]) intersect++;
    }


    QImage out = QImage(*im1);
    out.fill(0);
    for (int y=0; y<im1->height(); y++){
        int p = y * im1->width();
        for (int x=0; x<im1->width(); x++){
            if (arr2[p+x]) out.setPixel(x, y, qRgb(255, 255, 255));
        }
    }
    out.save("mask.tif", "tif");

    delete[] arr1;
    delete[] arr2;


    //the formula is 2*intersect / (size(arr1) + size(arr2))
    //but because the arrs have the same sizes, it can be shortened to
    //the following formula
    return (float)intersect / (float)el;
}



float Metrics::rmse(QImage *im1, QImage *im2){
    float res = 0;
    for (int y=0; y<im1->height(); y++){
        for (int x=0; x<im1->width(); x++){
            QRgb a = im1->pixel(x, y);
            QRgb b = im2->pixel(x, y);
            res += pow((float)qRed(a) - (float)qRed(b), 2.0);
            res += pow((float)qGreen(a) - (float)qGreen(b), 2.0);
            res += pow((float)qBlue(a) - (float)qBlue(b), 2.0);
        }
    }

    return sqrt(res/(3.0*(float)im1->width()*(float)im1->height()));
}



float Metrics::ssim(QImage *original, QImage *current){
    double c1, c2;
    double avg_o_r, avg_c_r, var_o_r, var_c_r, cov_r;
    double avg_o_g, avg_c_g, var_o_g, var_c_g, cov_g;
    double avg_o_b, avg_c_b, var_o_b, var_c_b, cov_b;
    c1 = c2 = 0;
    avg_o_r = avg_c_r = var_o_r = var_c_r = cov_r = 0;
    avg_o_g = avg_c_g = var_o_g = var_c_g = cov_g = 0;
    avg_o_b = avg_c_b = var_o_b = var_c_b = cov_b = 0;

    c1 = 6.5025;//pow(255 * 0.01, 2.0);
    c2 = 58.5225;//pow(255 * 0.03, 2.0);

    for (int x=0; x<original->width(); x++){
        for (int y=0; y<original->height(); y++){
            avg_o_r += qRed(original->pixel(x, y));
            avg_c_r += qRed(current->pixel(x, y));
            avg_o_g += qGreen(original->pixel(x, y));
            avg_c_g += qGreen(current->pixel(x, y));
            avg_o_b += qBlue(original->pixel(x, y));
            avg_c_b += qBlue(current->pixel(x, y));
        }
    }
    avg_o_r /= double(original->width()*original->height());
    avg_c_r /= double(current->width()*current->height());
    avg_o_g /= double(original->width()*original->height());
    avg_c_g /= double(current->width()*current->height());
    avg_o_b /= double(original->width()*original->height());
    avg_c_b /= double(current->width()*current->height());

    for (int x=0; x<original->width(); x++){
        for (int y=0; y<original->height(); y++){
            var_o_r += pow(qRed(original->pixel(x, y)) - avg_o_r, 2.0);
            var_c_r += pow(qRed(current->pixel(x, y))  - avg_c_r, 2.0);
            cov_r   += (qRed(current->pixel(x, y)) - avg_c_r) * (qRed(original->pixel(x, y)) - avg_o_r);
            var_o_g += pow(qGreen(original->pixel(x, y)) - avg_o_g, 2.0);
            var_c_g += pow(qGreen(current->pixel(x, y))  - avg_c_g, 2.0);
            cov_g   += (qGreen(current->pixel(x, y)) - avg_c_g) * (qGreen(original->pixel(x, y)) - avg_o_g);
            var_o_b += pow(qBlue(original->pixel(x, y)) - avg_o_b, 2.0);
            var_c_b += pow(qBlue(current->pixel(x, y))  - avg_c_b, 2.0);
            cov_b   += (qBlue(current->pixel(x, y)) - avg_c_b) * (qBlue(original->pixel(x, y)) - avg_o_b);
        }
    }
    var_o_r /= double(original->width()*original->height());
    var_c_r /= double(original->width()*original->height());
    cov_r   /= double(original->width()*original->height());
    var_o_r = sqrt(var_o_r);
    var_c_r = sqrt(var_c_r);
    var_o_g /= double(original->width()*original->height());
    var_c_g /= double(original->width()*original->height());
    cov_g   /= double(original->width()*original->height());
    var_o_g = sqrt(var_o_g);
    var_c_g = sqrt(var_c_g);
    var_o_b /= double(original->width()*original->height());
    var_c_b /= double(original->width()*original->height());
    cov_b   /= double(original->width()*original->height());
    var_o_b = sqrt(var_o_b);
    var_c_b = sqrt(var_c_b);

    float ssim_r = ((2*avg_o_r*avg_c_r+c1)*(2*cov_r+c2)) / ((pow(avg_o_r, 2.0)+pow(avg_c_r, 2.0)+c1)*(pow(var_o_r, 2.0)+pow(var_c_r, 2.0)+c2));
    float ssim_g = ((2*avg_o_g*avg_c_g+c1)*(2*cov_g+c2)) / ((pow(avg_o_g, 2.0)+pow(avg_c_g, 2.0)+c1)*(pow(var_o_g, 2.0)+pow(var_c_g, 2.0)+c2));
    float ssim_b = ((2*avg_o_b*avg_c_b+c1)*(2*cov_b+c2)) / ((pow(avg_o_b, 2.0)+pow(avg_c_b, 2.0)+c1)*(pow(var_o_b, 2.0)+pow(var_c_b, 2.0)+c2));

    return (ssim_r + ssim_g + ssim_b)/3.0;
}
