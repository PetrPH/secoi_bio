#ifndef STRUCTS_H
#define STRUCTS_H

#include <QList>
#include <QImage>
#include <QPoint>

#include <stdint.h>



typedef struct{
    QPoint pos;
    float arx;
    float ary;
    int rad;
    int type;
} history;



#endif // STRUCTS_H
