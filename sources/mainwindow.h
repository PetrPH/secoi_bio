#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QCursor>
#include <QColorDialog>

#include "image_painter.h"
#include "area_visualizer.h"
#include "metrics.h"

namespace Ui {
    class MainWindow;
}


class MainWindow : public QMainWindow{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private slots:
        void on_pushButton_clicked();
        void on_horizontalSlider_valueChanged(int value);
        void on_checkBox_saturation_toggled(bool checked);
        void on_checkBox_hue_toggled(bool checked);
        void on_checkBox_value_toggled(bool checked);
        void on_checkBox_soft_colors_toggled(bool checked);
        void on_pushButton_2_clicked();
        void on_checkBox_use_dist_toggled(bool checked);

        void on_pushButton_3_clicked();

private:
        Ui::MainWindow *ui;
        Image_painter *im_orig_label;
        Image_painter *im_mod_label;
        Area_visualizer *im_mod_area;
        Area_visualizer *im_mod_degree;
        bool first_open;
        QString fileName;
        QColor selected_color;
        bool selected_color_ready;
};

#endif // MAINWINDOW_H
