﻿#include "image_painter.h"

float Image_painter::count_area(){
    int area = 0;
    for (int x=0; x<im_area.width(); x++){
        for (int y=0; y<im_area.height(); y++){
            if ((im_area.pixel(x, y) & 0xff) == 255) area += 1;
        }
    }
    return 100.0 * float(area) / float(im_area.width() * im_area.height());
}


QImage Image_painter::export_final_output(){
    QPoint pos = QPoint(selected.x()*arx, selected.y()*ary);

    float arx2 = (float)im_orig.width()  / (float)im_orig_s.width();
    float ary2 = (float)im_orig.height() / (float)im_orig_s.height();

    //label_progress->setText("Rendering the final image, please wait.");
    QRect geom = groupBox_progress->geometry();
    progressBar_progress->setGeometry(QRect(10, 10, geom.width()-20, geom.height()-20));
    progressBar_progress->setFormat("Rendering the final image, please wait.");
    groupBox_progress->setVisible(true);
    groupBox_progress->raise();
    uint *data = Core_mockor::process(im_orig, pos.x()*arx2, pos.y()*ary2, progressBar_progress, hue, saturation, value, use_dist);
    QImage out =  Core_mockor::compose_image(im_orig, data, horizontalSlider_thresh->value(), soft_color);

    cout<<"all history size: "<<child->all_history.size()<<"\n";
    for (int i=0; i<child->all_history.size(); i++){
        //cout<<i<<"\t"<<child->all_history.at(i).pos.x()<<"\t"<<child->all_history.at(i).pos.y()<<"\t"<<child->all_history.at(i).type<<"\t"<<child->all_history.at(i).rad<<"\t"<<child->all_history.at(i).arx<<"\t"<<child->all_history.at(i).arx<<"\n";

        pos = child->all_history.at(i).pos;
        pos.setX(pos.x()*child->all_history.at(i).arx*arx2);
        pos.setY(pos.y()*child->all_history.at(i).ary*ary2);

        int v;
        int vradx = pos.x();
        int vrady = pos.y();

        int type  = child->all_history.at(i).type;
        int radx  = child->all_history.at(i).rad*child->all_history.at(i).arx*arx2;
        int rady  = child->all_history.at(i).rad*child->all_history.at(i).ary*ary2;

        if (type){
            for (int x=qMax(pos.x()-radx, 0); x<=qMin(pos.x()+rady,im_orig.width()-1); x++){
                for (int y=qMax(pos.y()-radx, 0); y<=qMin(pos.y()+rady,im_orig.height()-1); y++){
                    if (sqrt(pow(x-vradx,2.0)+pow(y-vrady,2.0))>radx) continue;
                    out.setPixel(x, y, im_orig.pixel(x, y));
                }
            }
        }
        else{
            for (int x=qMax(pos.x()-radx, 0); x<=qMin(pos.x()+rady,im_orig.width()-1); x++){
                for (int y=qMax(pos.y()-radx, 0); y<=qMin(pos.y()+rady,im_orig.height()-1); y++){
                    if (sqrt(pow(x-vradx,2.0)+pow(y-vrady,2.0))>radx) continue;
                    v = qGray(im_orig.pixel(x, y));
                    out.setPixel(x, y, qRgb(v, v, v));
                }
            }
        }
    }
    groupBox_progress->setVisible(false);

    delete[] data;
    return out;
}


void Image_painter::solve_moving(QPoint pos){
    history temp;
    temp.arx = arx;
    temp.ary = ary;
    temp.pos = pos;
    temp.rad = cursor_radius;
    temp.type = right;
    all_history.append(temp);


    pos.setX(pos.x()*arx);
    pos.setY(pos.y()*ary);

    int v;
    int vradx = pos.x();
    int vrady = pos.y();

    int radx = cursor_radius*arx;
    int rady = cursor_radius*ary;

    if (right){
        for (int x=qMax(pos.x()-radx, 0); x<=qMin(pos.x()+radx,im_act_s.width()-1); x++){
            for (int y=qMax(pos.y()-rady, 0); y<=qMin(pos.y()+rady,im_act_s.height()-1); y++){
                if (sqrt(pow(x-vradx,2.0)+pow(y-vrady,2.0))>radx) continue;
                im_act_s.setPixel(x, y, im_orig_s.pixel(x, y));
                im_area.setPixel(x, y, qRgb(255,255,255));
                im_degree.setPixel(x, y, qRgb(255,255,255));
            }
        }
    }
    else{
        for (int x=qMax(pos.x()-radx, 0); x<=qMin(pos.x()+radx,im_act_s.width()-1); x++){
            for (int y=qMax(pos.y()-rady, 0); y<=qMin(pos.y()+rady,im_act_s.height()-1); y++){
                if (sqrt(pow(x-vradx,2.0)+pow(y-vrady,2.0))>radx) continue;
                v = qGray(im_act_s.pixel(x, y));
                im_act_s.setPixel(x, y, qRgb(v, v, v));
                im_area.setPixel(x, y, qRgb(0,0,0));
                im_degree.setPixel(x, y, qRgb(0,0,0));
            }
        }
    }
    setPixmap(QPixmap::fromImage(im_act_s));
    area_visualizer->setPixmap(QPixmap::fromImage(im_area));
    degree_visualizer->setPixmap(QPixmap::fromImage(im_degree));
    statusBar->showMessage("Volume of selected area: " + QString::number(qRound(count_area()*100) / 100.0, 'f', 2)+" %");
}


void Image_painter::solve_click(QColor selected_color){
    if (!selected_color.isValid()){
        QColor col = im_orig_s.pixel(selected.x()*arx, selected.y()*ary);
        QString qss = QString("background-color: %1").arg(col.name());
        button_with_color_bcg->setStyleSheet(qss);
        *selected_color_parent = col;
    }

    QRect geom = centralWidget->geometry();
    groupBox_progress->setGeometry(QRect(geom.width()/4, geom.height()/2-40, geom.width()/2, 80));

    geom = groupBox_progress->geometry();
    progressBar_progress->setGeometry(QRect(10, 10, geom.width()-20, geom.height()-20));
    //label_progress->setGeometry(QRect(10, 10, geom.width()-20, 21));
    progressBar_progress->setFormat("Preparing data, please wait.");

    groupBox_progress->setVisible(true);
    groupBox_progress->raise();
    progressBar_progress->setValue(0);
    processed        = Core_mockor::process(im_orig_s, selected.x()*arx, selected.y()*ary, progressBar_progress, hue, saturation, value, use_dist, selected_color);
    ready            = true;
    child->ready     = true;
    child->im_act_s  = Core_mockor::compose_image(im_orig_s, processed, horizontalSlider_thresh->value(), soft_color);
    child->im_area   = Core_mockor::compose_area(im_orig_s, processed, horizontalSlider_thresh->value());
    child->im_degree = Core_mockor::compose_degree(im_orig_s, processed, horizontalSlider_thresh->value());
    child->setPixmap(QPixmap::fromImage(child->im_act_s));
    area_visualizer->setPixmap(QPixmap::fromImage(child->im_area));
    degree_visualizer->setPixmap(QPixmap::fromImage(child->im_degree));
    statusBar->showMessage("Volume of selected area: " + QString::number(qRound(child->count_area()*100) / 100.0, 'f', 2)+" %");
    groupBox_progress->setVisible(false);
}


void Image_painter::mousePressEvent(QMouseEvent *event){
    emit onClick();

    QPoint pos = QWidget::mapFromGlobal(QCursor::pos());
    selected   = pos;
    clicked    = true;

    arx        = float(im_orig_s.width()) / float(this->width());
    ary        = float(im_orig_s.height()) / float(this->height());
    cout<<"aspect ratio: "<<arx<<"\t"<<ary<<"\n";
    cout<<this->width()<<"\t"<<this->height()<<"\n";


    if (is_master && im_orig.width()){
        solve_click();
    }

    if (!is_master && ready){
        if (event->button() == Qt::RightButton) right = true;
        else                                    right = false;
        solve_moving(pos);
    }
}




void Image_painter::mouseReleaseEvent( QMouseEvent *){
    emit onClick();
    clicked = false;
}


void Image_painter::mouseMoveEvent(QMouseEvent *){
    emit onClick();
    QPoint pos = QWidget::mapFromGlobal(QCursor::pos());

    if (!clicked) return;

    if (!is_master && ready){
        solve_moving(pos);
        //setPixmap(QPixmap::fromImage(im_act_s));
    }
}


void Image_painter::wheelEvent(QWheelEvent *event){
    if (is_master) return;
    cout<<event->delta()<<"\n";

    QCursor cursor;
    if (event->delta() < 0){
        if (cursor_type==2) return;
        cursor_type--;

    }
    else{
        if (cursor_type==12) return;
        cursor_type++;
    }
    cursor        = QCursor(QPixmap::fromImage(QImage("resources/cursor_circle_"+QString::number(cursor_type*4)+".png")), cursor_type*2-1, cursor_type*2-1);
    cursor_radius = cursor_type*2-1;
    this->setCursor(cursor);
}



Image_painter::Image_painter(QWidget * parent): QLabel(parent) {
    this->ready         = false;
    this->parent        = parent;
    this->clicked       = false;
    this->right         = false;
    this->hue           = true;
    this->saturation    = true;
    this->value         = true;
    this->soft_color    = false;
    this->use_dist      = false;
    this->cursor_type   = 8;
    this->cursor_radius = 16;
    this->setVisible(true);
    this->setFrameShape(QFrame::Box);
}


Image_painter::Image_painter( const QString& text, QWidget* parent, Qt::WindowFlags f ): QLabel( text, parent, f ) {
    srand(0);
}

