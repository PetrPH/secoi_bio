#ifndef AREA_VISUALIZER_H
#define AREA_VISUALIZER_H

#include <QMainWindow>
#include <QtGui>
#include <QLabel>
#include <QWidget>

class Area_visualizer : public QLabel{
    Q_OBJECT
    public:
        Area_visualizer( QWidget* parent = 0);
        Area_visualizer( const QString& text, QWidget* parent = 0, Qt::WindowFlags f = 0 );

    private:
        QWidget* parent;
};

#endif // AREA_VISUALIZER_H
