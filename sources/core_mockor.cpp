#include "core_mockor.h"


inline qreal absf(qreal a, qreal b){
    return a-b < 0 ? -(a-b) : a-b;
}

inline qreal absf(qreal a){
    return a < 0 ? -a : a;
}


float Core_mockor::m_location(int x1, int y1, int x2, int y2){
    return 1.0 / pow(2.0, (abs(x1-x2) + abs(y1-y2))>>1 );
}

float Core_mockor::m_dist(int x1, int y1, int x2, int y2, int w, int h){
    float a = 1.0-(float)abs(x1-x2)/(float)w;
    float b = 1.0-(float)abs(y1-y2)/(float)h;
    return qMin(a,b);
}


float Core_mockor::m_color(float sear0, float sear1, float sear2, float act0, float act1, float act2, bool hue, bool saturation, bool value){
    if (sear0<-0.5 || act0<-0.5) return 0.0;
    sear0 -= 127.5;
    act0  -= 127.5;

    float h1 = 1.0-absf(sear0-act0)/255.0;
    float h2 = 1.0-float(127.5-absf(sear0) + 127.5-absf(act0))/255.0;

    float d1 = (qMax(h1, h2)-0.5)*2.0;
    float d2 = 1.0-absf(sear1,act1);
    float d3 = 1.0-absf(sear2,act2);

    return (d1*hue + d2*saturation + d3*value) / float(hue+saturation+value);
}

uint* Core_mockor::process(QImage im, int posx, int posy, QProgressBar *progress, bool hue, bool saturation, bool value, bool use_dist, QColor selected){
    QTime myTimer;
    myTimer.start();

    int rad     = 5;
    int width   = im.width();
    int height  = im.height();
    int width1  = width-1;
    int height1 = height-1;
    uint *out   = new uint[width*height];

    QColor col;
    float sum, num, color, location, sear0, sear1, sear2, dist;
    int p, pp;
    QList<int> act;
    act<<0<<0<<0;

    float *im_hsv0 = new float[width*height];
    float *im_hsv1 = new float[width*height];
    float *im_hsv2 = new float[width*height];

    for (int y=0; y<height; y++){
        p=y*width;
        for (int x=0; x<width; x++){
            col = im.pixel(x, y);
            col.getHsv(&act[0], &act[1], &act[2]);
            im_hsv0[p]= float(act.at(0)) / 1.42;
            im_hsv1[p]= float(act.at(1)) / 255.0;
            im_hsv2[p]= float(act.at(2)) / 255.0;
            p++;
        }
    }

    if (!selected.isValid()){
        p     = posx + posy * im.width();
        sear0 = im_hsv0[p];
        sear1 = im_hsv1[p];
        sear2 = im_hsv2[p];
    }
    else{
        sear0 = (float)selected.hue() / 1.42;
        sear1 = (float)selected.saturation() / 255.0;
        sear2 = (float)selected.value() / 255.0;
        posx = 0;
        posy = 0;
    }


    for (int y=0; y<im.height(); y++){
        if (!(y%10))progress->setValue(float(y)/float(height)*100.0);
        pp=y*im.width();
        for (int x=0; x<width; x++){
            sum  = 0;
            num  = 0;
            dist = m_dist(x, y, posx, posy, im.width(), im.height());


            for (int yy=qMax(y-rad,0); yy<qMin(y+rad, height1); yy++){
                p = yy*im.width();
                for (int xx=qMax(x-rad,0); xx<qMin(x+rad, width1); xx++){
                    color      = m_color(sear0, sear1, sear2, im_hsv0[p+xx], im_hsv1[p+xx], im_hsv2[p+xx], hue, saturation, value);
                    location   = m_location(x, y, xx, yy);
                    sum       += color * location;
                    num       += location;
                }
            }
            if (use_dist) out[pp] = sum/num*255.0*dist;
            else          out[pp] = sum/num*255.0;

            pp++;
        }
    }

    cout<<"done in "<<myTimer.elapsed()<<"ms\n";

    return out;
}



QImage Core_mockor::compose_image(QImage im, uint* data, int thresh, bool soft_color){
    float dg, dg1, v;
    int degree, p;

    QImage temp = QImage(im);
    for (int y=0; y<im.height(); y++){
        p=y*im.width();
        for (int x=0; x<im.width(); x++){
            degree = data[p];
            p++;
            if (degree > thresh){
                if (!soft_color) degree = 255;
                dg  = (degree / 255.0);
                dg1 =  1.0-dg;
                v = qGray(im.pixel(x, y));
                temp.setPixel(x, y, qRgb(
                                 (float)qRed(im.pixel(x, y))*dg + v*dg1,
                                 (float)qGreen(im.pixel(x, y))*dg + v*dg1,
                                 (float)qBlue(im.pixel(x, y))*dg + v*dg1
                                 ));
            }
            else{
                v = qGray(im.pixel(x, y));
                temp.setPixel(x, y, qRgb(v, v, v));
            }
        }
    }
    return temp;
}



QImage Core_mockor::compose_area(QImage im, uint* data, int thresh){
    int degree, p;

    QImage temp = QImage(im);
    for (int y=0; y<im.height(); y++){
        p=y*im.width();
        for (int x=0; x<im.width(); x++){
            degree = data[p];
            p++;
            if (degree > thresh) temp.setPixel(x, y, qRgb(255,255,255));
            else                 temp.setPixel(x, y, qRgb(0, 0, 0));
        }
    }
    return temp;
}


QImage Core_mockor::compose_degree(QImage im, uint* data, int thresh){
    int degree, p, val;
    float mult = 255.0/(255.0-thresh);

    QImage temp = QImage(im);
    for (int y=0; y<im.height(); y++){
        p=y*im.width();
        for (int x=0; x<im.width(); x++){
            degree = data[p];
            p++;
            if (degree > thresh){
                val = int((degree-thresh)*mult);
                temp.setPixel(x, y, qRgb(val,0,255-val));
            }
            else temp.setPixel(x, y, qRgb(0, 0, 0));
        }
    }
    return temp;
}
